﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace LudumDare34
{
    public class LudumDare
    {
        public Texture2D Warning { get; set; }
        public Texture2D AUp { get; set; }
        public Texture2D ADown { get; set; }
        public Texture2D DUp { get; set; }
        public Texture2D DDown { get; set; }
        public Texture2D EndScreen { get; set; }
        public Texture2D Background { get; set; }
        public Texture2D MousePointer { get; set; }

        public SpriteFont Arial { get; set; }

        public Rectangle ARect { get; set; }
        public Rectangle DRect { get; set; }

        public Random Rand { get; set; }
        public Dictionary<string, string> RacistQuotes { get; set; }
        public ContentManager Content { get; set; }

        public string NextQuote { get; set; }
        public string Reply { get; set; } = "";

        public uint Drunkness { get; set; }
        public int Count { get; set; } = 0;

        public KeyboardState kb { get; set; }
        public KeyboardState preKb { get; set; }

        public MouseState ms { get; set; }
        public MouseState prevMs { get; set; }

        public bool isWarning { get; set; } = true;
        public bool isGame { get; set; }
        public bool isUI { get; set; } = true;
        public bool isSecond { get; set; }

        float threeSecTimer = 0;

        public LudumDare(ContentManager content)
        {
            Content = content;

            Warning = Content.Load<Texture2D>("Warning");
            Background = Content.Load<Texture2D>("Family");
            EndScreen = Content.Load<Texture2D>("EndScreen");
            Arial = Content.Load<SpriteFont>("Arial");
            MousePointer = Content.Load<Texture2D>("MousePointer");

            AUp = Content.Load<Texture2D>("AUp");
            ADown = Content.Load<Texture2D>("ADown");

            DUp = Content.Load<Texture2D>("DUp");
            DDown = Content.Load<Texture2D>("DDown");

            //ARect = new Rectangle(230, 400, 50, 50);
            //DRect = new Rectangle(300, 400, 50, 50);
            ARect = new Rectangle(640 / 2 - 130, 80, 50, 50);
            DRect = new Rectangle(640 / 2 + 130, 80, 50, 50);

            Rand = new Random();
            RacistQuotes = new Dictionary<string, string>();

            RacistQuotes["Those darn orange people are taking over the place. \nPurple master race!"] = "Shut it Grandad! Or I'll paint you orange.";
            RacistQuotes["*There is a knock at the door.* Leave us in peace you blue bastards! \nGo on and disturb the teletubbies!"] = "Come on Grandad what's wrong with Teletubbies, \nyou know I loved that show when I was younger.";
            RacistQuotes["You see the football last night? Fucking oranges won."] = "Hey Grandad, you ever been for a spraytan?";
            RacistQuotes["Boy! Why didn't you get that job? I bet an orange man took it! \nWe should kill him!"] = "Chill, Grandad, he's just the same as us... except he's from Essex.";
            RacistQuotes["A blue girl pushed infront of me today at Tesco. Fuckers."] = "I don't think I should let you meet my girlfriend.";
            RacistQuotes["Green men are a disease!"] = "Hey grandad did grandma ever tell you about her past boyfriends?";

            GetNextQuote();
        }

        public string GetNextQuote()
        {
            List<string> RacistKeys = new List<string>(RacistQuotes.Keys);
            NextQuote = RacistKeys[Rand.Next(0, RacistQuotes.Count)];
            return NextQuote;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (isWarning)
            {
                spriteBatch.Draw(Warning, new Vector2(0, 0), Color.White);
            }
            if (isGame)
            {
                spriteBatch.Draw(Background, Vector2.Zero, Color.White);

                if (isUI)
                {
                    if (Drunkness <= 100) { spriteBatch.DrawString(Arial, "Drunkness: " + Drunkness + "/100", new Vector2(10, 10), Color.White); }
                    if (NextQuote != "") { spriteBatch.DrawString(Arial, "Grandad: " + NextQuote, new Vector2((640 / 2) - 250, 30), Color.White); }
                    //spriteBatch.DrawString(Arial, "A or D?", new Vector2(640 / 2, 80), Color.White);
                    if (Reply != null || Reply.Length > 0) { spriteBatch.DrawString(Arial, "You: " + Reply, new Vector2((640 / 2) - 200, 150), Color.White); }
                    spriteBatch.Draw(AUp, ARect, Color.White);
                    spriteBatch.Draw(DUp, DRect, Color.White);
                    spriteBatch.Draw(MousePointer, new Vector2(Mouse.GetState().X, Mouse.GetState().Y), Color.White);
                }
            }

        }

        public void Update(GameTime gameTime)
        {
            threeSecTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;

            kb = Keyboard.GetState();
            ms = Mouse.GetState();

            if (threeSecTimer >= 10)
            {
                isWarning = false;
                isGame = true;
            }

            if (isGame)
            {
                if (ARect.Contains(ms.X, ms.Y) && ms.LeftButton == ButtonState.Pressed && prevMs.LeftButton == ButtonState.Released)
                {
                    Reply = RacistQuotes[NextQuote];

                    if (Count == 5)
                    {
                        Reply = "Your grandad's sheer stupidity calls for a drink.";
                        Drunkness += 10;
                        Count = 0;
                    }

                    Count += 1;
                }

                else if (DRect.Contains(ms.X, ms.Y) && ms.LeftButton == ButtonState.Pressed && prevMs.LeftButton == ButtonState.Released && Drunkness < 100)
                {
                    Reply = "Fucksake Grandad you're so old fashioned. *Drinks*";
                    Drunkness += 10;
                }

                if (kb.IsKeyDown(Keys.Enter) && preKb.IsKeyUp(Keys.Enter))
                {
                    Reply = "";
                    NextQuote = "";
                    GetNextQuote();
                }

                if (Drunkness >= 100)
                {
                    Reply = "";
                    NextQuote = "";
                    isUI = false;
                    Background = EndScreen;
                }
            }

            preKb = kb;
            prevMs = ms;
        }
    }
}
